start:
	cd docker/traefik && docker-compose up -d
	cd docker/whoami && docker-compose up -d
	docker network connect whoami traefik

stop:
	cd docker/traefik && docker-compose stop
	cd docker/whoami && docker-compose stop
	docker network prune

ip:
	docker inspect whoami | grep IPAddress
	docker inspect traefik | grep IPAddress