
# Edit /etc/hosts

```
sudo nano /etc/hosts
```

```
172.66.122.10   traefik.local
172.66.122.10   whoami.local
```

# Install make

```
sudo apt-get install make
```

# Start

```
make start
```

# Stop

```
make stop
```